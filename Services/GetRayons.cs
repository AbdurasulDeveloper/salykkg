﻿using SalykKg.Models;
using Newtonsoft.Json;
using System.Text;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace SalykKg.Services
{
    public interface IGetRayons
    {
        public Task<RayonResponse> GetRayonsByINN(string Tin);
        public Task<List<CountrySide>> GetCountrySide(string rayonCode);
        public Task<List<TaxType>> GetTaxType();
        public Task<List<BudgetClassification>> GetBudgetClassification();
        public Task<TaxPayerDebt> GetTaxPayerDebt();
        public Task<TaxPayment> PostTaxPayment(List<TaxPaymentRequest> taxPaymentRequest, string inn);
        public Task<Status> CheckStatus(string transactionId);
    }
    public class GetRayons : IGetRayons
    {
        private readonly IConfiguration configuration;
        public GetRayons(IConfiguration configuration)
        {
            this.configuration = configuration;
        }
        public async Task<RayonResponse> GetRayonsByINN(string Tin)
        {
            string url = configuration.GetSection("Url").Get<string>() + "dictionary/taxpayer-rayons?Tin=" + Tin;
            var client = getHttpClient();
            HttpResponseMessage response = await client.GetAsync(url);
            string responseBody = await response.Content.ReadAsStringAsync();
            RayonResponse myDeserializedClass = JsonConvert.DeserializeObject<RayonResponse>(responseBody);
            return myDeserializedClass;
        }

        public async Task<List<CountrySide>> GetCountrySide(string rayonCode)
        {
            string url = configuration.GetSection("Url").Get<string>() + "dictionary/taxpayer-countryside?rayonCode=" + rayonCode;
            var client = getHttpClient();
            HttpResponseMessage response = await client.GetAsync(url);
            string responseBody = await response.Content.ReadAsStringAsync();
            List<CountrySide> myDeserializedClass = JsonConvert.DeserializeObject<List<CountrySide>>(responseBody);
            return myDeserializedClass;
        }

        public async Task<List<TaxType>> GetTaxType()
        {
            string url = configuration.GetSection("Url").Get<string>() + "dictionary/tax-types";
            var client = getHttpClient();
            HttpResponseMessage response = await client.GetAsync(url);
            string responseBody = await response.Content.ReadAsStringAsync();
            TaxTypeResponse myDeserializedClass = JsonConvert.DeserializeObject<TaxTypeResponse>(responseBody);
            return myDeserializedClass.data;
        }

        public async Task<List<BudgetClassification>> GetBudgetClassification()
        {
            string url = configuration.GetSection("Url").Get<string>() + "dictionary/budget-classifications";
            var client = getHttpClient();
            HttpResponseMessage response = await client.GetAsync(url);
            string responseBody = await response.Content.ReadAsStringAsync();
            BudgetClassificationResponse myDeserializedClass = JsonConvert.DeserializeObject<BudgetClassificationResponse>(responseBody);
            return myDeserializedClass.data;
        }
        public async Task<TaxPayerDebt> GetTaxPayerDebt()
        {
            return new TaxPayerDebt()
            {
                taxCode = "1170",
                taxName = "Налог на деятельность в сфере электронной коммерции",
                budgetClassifications = new List<BudgetClassForDebt>()
                {
                    new BudgetClassForDebt()
                    {
                        chapterCode = "11126100",
                        chapterName = "Налог на деятельность в сфере электронной коммерции"
                    }
                },
                totalSum = 6317.37
            };
        }

        public async Task<TaxPayment> PostTaxPayment(List<TaxPaymentRequest> t, string inn)
        {
            string url = configuration.GetSection("Url").Get<string>() + "taxpayment?Tin=" + inn;
            var client = getHttpClient();
            var payload = JsonSerializer.Serialize(t);
            var content = new StringContent(payload, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(url, content);
            string responseBody = await response.Content.ReadAsStringAsync();
            TaxPaymentResponse myDeserializedClass = JsonConvert.DeserializeObject<TaxPaymentResponse>(responseBody);
            return myDeserializedClass.data;
        }

        public async Task<Status> CheckStatus(string transactionId)
        {
            string url = configuration.GetSection("Url").Get<string>() + "taxpayment/check-status?transactionId=" + transactionId;
            var client = getHttpClient();
            HttpResponseMessage response = await client.GetAsync(url);
            string responseBody = await response.Content.ReadAsStringAsync();
            StatusResponse myDeserializedClass = JsonConvert.DeserializeObject<StatusResponse>(responseBody);
            return myDeserializedClass.data;
        }

        public HttpClient getHttpClient()
        {
            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("X-Road-Client", "central-server/COM/60000039/esb_system");
            client.DefaultRequestHeaders.Add("UserId", "8be2fc67-02b2-4f55-969e-09a4d26a5390");
            client.DefaultRequestHeaders.Add("Id", "156453");
            return client;
        }

    }
}
