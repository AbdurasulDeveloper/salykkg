﻿namespace SalykKg.Models
{
    public class GeneralResponse
    {
        public bool succeed { get; set; }
        public string message { get; set; }
    }
    public class Rayon
    {
        public string id { get; set; }
        public string displayText { get; set; }
        public string displayTextWithCode { get; set; }
    }
    public class RayonResponse : GeneralResponse
    {
        public List<Rayon> data { get; set; }
    }
    public class CountrySide
    {
        public string codeSts { get; set; }
        public string codeSf { get; set; }
        public string name { get; set; }
        public bool isParent { get; set; }
        public string parentCodeSts { get; set; }
    }
    public class TaxType
    {
        public string taxCode { get; set; }
        public string text { get; set; }
        public string shortText { get; set; }
        public string shortTextWithCode { get; set; }
    }
    public class TaxTypeResponse : GeneralResponse
    {
        public List<TaxType> data { get; set; }
    }
    public class BudgetClassification
    {
        public string taxCode { get; set; }
        public string chapterCode { get; set; }
        public string chapterName { get; set; }
    }
    public class BudgetClassificationResponse
    {
        public List<BudgetClassification> data { get; set; }
    }
    public class BudgetClassForDebt
    {
        public string chapterCode { get; set; }
        public string chapterName { get; set; }
    }
    public class TaxPayerDebt
    {
        public string taxCode { get; set; }
        public string taxName { get; set; }
        public List<BudgetClassForDebt> budgetClassifications { get; set; }
        public double totalSum { get; set; }
    }
    public class TaxPayment
    {
        public string transactionId { get; set; }
        public string paymentCode { get; set; }
    }
    public class TaxPaymentResponse : GeneralResponse
    {
        public TaxPayment data { get; set; }
    }
    public class TaxPaymentRequest
    {
        public string rayonCode { get; set; }
        public string rayonName { get; set; }
        public string countrySideCode { get; set; }
        public string countrySideName { get; set; }
        public string taxCode { get; set; }
        public string taxName { get; set; }
        public string chapterCode { get; set; }
        public string chapterName { get; set; }
        public double taxAmount { get; set; }
    }

    public class Status
    {
        public int transactionStatus { get; set; }
        public string payCode { get; set; }
        public double amount { get; set; }
        public double underpaymentAmount { get; set; }
        public DateTime lastPaymentDateTime { get; set; }
        public double paidAmount { get; set; }
    }
    public class StatusResponse : GeneralResponse
    {
        public Status data { get; set; }
    }
}
